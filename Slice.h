
#ifndef Slice_H
#define Slice_H
#include <string>
#include "FreeImage.h"

using std::string;

class Slice {
public:
  Slice();
  void Allocate(unsigned width, unsigned height);
  void Allocate(int z, unsigned width, unsigned height) { num = z; Allocate(width, height); }
  ~Slice();
  //! Get image bitmap from slice
  float * GetBits();
  //! Slices saver
  /*!
  Save slices to path from settings.ini
  */
  double Save(string & pathToSave, int z) { path = pathToSave; return Save(z); }
  double Save(int z);
  double Save();
  //! Whether to create small size previews
  /*!
  Preview: rescale twice, convert to 8-bit greyscale and save as jpeg
  */
  void Num(int n) { num = n; }
  int Num() { return num; }
  static void Path(string pathToSave) { path = pathToSave; }
  static void Path(const char * pathToSave) { path = pathToSave; }
  static string Path() { return path; }
  static void MakePreviews(bool b) { makePreviews = b; }
private:
  int num;
  static string path;
  FIBITMAP * dib;
  static bool makePreviews;
  void CreatePreview(string preview_name);
};

#endif
