
#ifndef FDK_H
#define FDK_H
#include <string>
#include <vector>
#include <omp.h>
#include "globals.h"

//! FDK algorithm class.
/*!
 Some realization of FDK algorithm in one class.
 Main functional: synograms input, interpolation correction, synograms transformation to slices, slices output.
*/

using std::string;
using std::vector;

class Slice;

class FDK {
public:
  //! A modern constructor.
  /*!
  Provides initialization of main algorithm variables.
  \param [in] settings_file
  accepts file with settings
  */
  static void Init(string settings_file = "settings.ini",
      bool use_host = true, bool use_mic = true);
  //! A destructor.
  /*!
  Free memory
  */
  static void Finalize();
  //! Main function of algorithm
  /*!
  Initiator of algorithm
  */
  static void Reconstruct();
private:
  //! Reconstructs single slice.
  static void ReconstructSlice(int z, int b1, int b2);
  //! Synogram opener.
  /*!
  Open set of synograms with some interpolation
  */
  static void OpenSynograms(unsigned int dev, int b1, int b2);
  static void CloseSynograms();
  //! Pre-calculations
  /*!
    Some calculation to initialite algorithm
  */
  static void SingleCalculations();
  //! functions for offload
__declspec(target(mic))
  inline static double Buf(float * b, int x, int y);
__declspec(target(mic))
  static void ReconstructBitmap(int z, int b1, int b2, float * bitmap);

//! variables shared with coprocessor
__declspec(target(mic))
  static int x0, y0, halfx0, halfy0, /*! output picture sizes */
             rap2, halfw, w, /*! sizes of expanded detector */
             p3, p3p2; /*! p array sizes */
__declspec(target(mic))
  static float rap, R, theta, sintheta, costheta, /*! geometrical parameters */
               HorCentre, VertCentre; /*! Horizontal and vertical centeres */
__declspec(target(mic))
  static unsigned int Xmax, Ymax, /*! sizes of changed input picture */
                      width, height; /*! sizes of input pictures */
__declspec(target(mic))
  static float * angle, /*! array with angles */
               * filter, /*! ramp filter */
//                * p, /*! preprocessed input data */
               * kp,
               * Ffdk, /*! bitmap with inverted axes */
               * b [DETH]; /*! input bitmaps */

//! host-only variables
  static bool ** in_use;
  static Slice * slices;

  static int Z1, Z2; /*! slices for reconstruction */

  static float sdd, sod, pixelsize; /*! geometrical parameters */

  static string PathToScan, PathToAngles, PathToSave; /*! paths from scanner */
  static vector <string> files; /*! vector of files from PathToScan */
  static double tmFDK;
  static double tmOpen, tmSave, tmPre, tmConv, tmRec, tmSlice, tmAlg;
  static bool USE_HOST, USE_MIC;
  static unsigned int n_mic, n_dev;
  static int z_host, * z_mic;
  static char * mic_sig;
  static omp_lock_t host_lock;
};
__declspec(target(mic))
inline double FDK::Buf(float * b, int x, int y) {
  x -= EDGE; y -= EDGE;
  if(x < 0 || x >= width) return 0;
  if(y < 0 || y >= height) return 0;
  return b[(height-1-y)*width+x];
}
/*
class Buffer {
public:
  Buffer(float * bits, int width, int height): zero(0), b(bits), w(width), h(height) { ; }
  float & operator()(int x, int y) {
    x -= EDGE; y -= EDGE;
    if(x < 0 || x >= w) return zero;
    if(y < 0 || y >= h) return zero;
    return b[(h-1-y)*w+x];
  }
private:
  float zero;
  float * b;
  int w, h;
};
*/

#endif // FDK_H
