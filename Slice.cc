
#include "Slice.h"
#include "globals.h"
#include <iostream>
#include <omp.h>

using std::cout;
using std::cerr;
using std::endl;

string Slice::path = ".";

Slice::Slice(): num(0), dib(0) { ; }
void Slice::Allocate(unsigned width, unsigned height) {
  if(dib) FreeImage_Unload(dib);
  dib = FreeImage_AllocateT(FIT_FLOAT, width, height);
  if (!dib) {
    cerr << "ImgSave::Cant`t create FreeImage bitmap" << endl;
  }
}
Slice::~Slice() {
  if(dib) FreeImage_Unload(dib);
}
bool Slice::makePreviews = true;
double Slice::Save(int z) {
  num = z;
  return Save();
}
double Slice::Save() {
  if(!dib) return 0;
  double save = omp_get_wtime();
  char c[4]; snprintf(c, 4, "%03i", num);
  string Filename = path + "/" + c + ".tif";
  bool save_success = FreeImage_Save(FIF_TIFF, dib, Filename.c_str(), TIFF_DEFLATE/*TIFF_NONE*/);
  if (!save_success) {
    cerr << "ImgSave::Can`t save bitmap" << endl;
  }
  if(makePreviews) CreatePreview(path+"/preview_"+c);
  FreeImage_Unload(dib);
  dib = 0;
  save = omp_get_wtime()-save;
  if(verbose_timing_info) cout<<"Save time "<<save<<endl;
  return save;
}
float * Slice::GetBits() {
  if(!dib) return 0;
  float * bin = reinterpret_cast<float*>(FreeImage_GetBits(dib));
  if (bin == 0) {
    cerr << "File has width or height equal to zero or NULL pointer" << endl;
    return 0;
  }
  return bin;
}
void Slice::CreatePreview(string preview_name) {
  if(!dib) return;
  double prev = omp_get_wtime();
  float * bin = GetBits();
  unsigned w = FreeImage_GetWidth(dib)/2;
  unsigned h = FreeImage_GetHeight(dib)/2;
  FIBITMAP * dtb = FreeImage_AllocateT(FIT_FLOAT, w, h);
  if (!dtb) {
    cerr << "ImgSave::Cant`t create FreeImage bitmap" << endl;
    return;
  }
  float * bout = reinterpret_cast<float*>(FreeImage_GetBits(dtb));
  for(unsigned x = 0; x < w; x++)
    for(unsigned y = 0; y < h; y++) {
      unsigned iout = y*w+x;
      unsigned iin = y*4*w+x*2;
      bout[iout] += bin[iin];
      bout[iout] += bin[iin+1];
      bout[iout] += bin[iin+2*w];
      bout[iout] += bin[iin+2*w+1];
      bout[iout] /= 4.;
    }
  FIBITMAP * dob = FreeImage_ConvertToStandardType(dtb);
  FreeImage_Unload(dtb);
  if (!FreeImage_Save(FIF_JPEG, dob, (preview_name+".jpg").c_str(), JPEG_DEFAULT)) {
    cerr << "ImgSave::Can`t save bitmap" << endl;
    return;
  }
  FreeImage_Unload(dob);
  if(verbose_timing_info) cout<<"CreatePreview time "<<omp_get_wtime()-prev<<endl;
}
