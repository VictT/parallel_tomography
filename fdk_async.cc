
#include "fdk.h"
#include <offload.h>
#include <cmath>
#include <cstring>
// #ifndef __MIC__
//   #include <iostream>
//   #include <cstdio>
//   using std::cout;
//   using std::cerr;
//   using std::endl;
//   using std::flush;
// #endif

__declspec(target(mic))
void FDK::ReconstructBitmap(int z, int b1, int b2, float * bitmap/*,
        double & preTime, double & convTime, double & recTime*/) {
  double sliceTime = omp_get_wtime(), preTime = omp_get_wtime(), convTime = 0, recTime = 0;
  int h = b2-b1;
  h = abs(h-2*VertCentre) + h; //new height of the detector
  int halfh = 0.5*(h-1); // halfheight
  float * p = reinterpret_cast<float*>(calloc((b2-b1)*p3p2, sizeof(float)));
/*  #ifndef __MIC__
    cout<<(b2-b1)*p3p2<<" "<<b2-b1<<" "<<Ymax<<" "<<p3<<endl;
  #endif*/
  for (int I = b1; I < b2; I++) {
    float c = R*R + (I-halfh)*(I-halfh); //part of PreweightFactor
    // PreweightFactor = R/sqrt(R*R+a*a+b*b)
    #pragma omp parallel for
    for (int i = 0; i <= halfw*2+1; i++) {
      kp[i] = R/sqrt(c + (i-halfw)*(i-halfw));
    }
    #pragma omp parallel for
    for (unsigned int i = 0; i < Ymax; i++) {
      float y = i/sintheta + EDGE;
      int y1 = static_cast<int>(y);
      y -= y1; // fractional part
      for (unsigned int j = 0; j < Xmax; j++) {
        float x = j/sintheta + EDGE; //input data transform
        int x1 = static_cast<int>(x);
        x -= x1; // fractional part
        int idx = static_cast<int>(j+0.5*w - rap);
/*        #ifndef __MIC__
          printf("%i %i %i %i\n",(I-b1)*p3p2 + i*p3 + idx,I-b1,i,idx);
          cout<<flush;
          cout<<p[(I-b1)*p3p2 + i*p3 + idx]<<endl;
          cout<<kp[idx]<<endl;
          cout<<Buf(b[I],x1,y1)<<" "<<x<<" "<<y<<endl;
        #endif*/
        //Linear interpolation
        p[(I-b1)*p3p2 + i*p3 + idx]
            = -kp[idx]
            * log(Buf(b[I],x1,y1)*(1-x)*(1-y)
                + Buf(b[I],x1+1,y1)*x*(1-y)
                + Buf(b[I],x1,y1+1)*(1-x)*y
                + Buf(b[I],x1+1,y1+1)*x*y);
      }
    }
  }
  preTime = omp_get_wtime()-preTime;
  //Free FreeImage's copy of the data
//   FreeImage_Unload(dib);
//   is_opened[I] = true;
//   PreprocessInput()
  /// Ffdk x0 y0 rap halfw Xmax b2 b1 Ymax R angle[] w p[] filter[] halfx0 z halfh halfy0
  /// STATIC  : Ffdk x0 y0 rap halfw Xmax Ymax R angle[] w p[] filter[] halfx0 halfy0
  /// DYNAMIC : b2 b1 z halfh
  memset(Ffdk, 0, x0*y0*sizeof(float));
  const int idx_l = halfw-rap2, idx_u = halfw+Xmax-rap2;
  const int wconv = b2-b1;
//   convTime = 0, recTime = 0; // reduction(+:convTime,recTime)
  #pragma omp parallel for
  for (unsigned int B = 0; B < Ymax; B++) {
    float sinb = sin(angle[B]);
    float cosb = cos(angle[B]);
    float RRAnglePitch = R*R*(angle[B+1]-angle[B]);
    float * conv = (float*)calloc(w*wconv, sizeof(float));
    double start = omp_get_wtime();
    for (int b = 0; b < wconv; b++) { //b1, b2 are limits of integration
      // Начинается свертка.
      float * tp = &p[(/*b1+*/b)*p3p2+B*p3];
      for (int a = -halfw; a <= halfw; a++) {
        float & tconv = conv[(a+halfw)*wconv+b];
        float * tfilter = &filter[halfw-a];
        for(int index = idx_l+a%2; index < idx_u; index++) {
          //index и a дожны быть разной четности
          //В массив p записывает свертка с фильтром 4*halfw+1
          tconv += tp[index]*tfilter[index];
        }
      }
    }
    convTime += omp_get_wtime()-start;
    start = omp_get_wtime();
    for (int x = -rap2; x < rap2; x++) {
      int recrad = sqrt((rap-x)*(rap+x)); // radius of reconstructed circle
      float * tFfdk = &Ffdk[(halfx0+x)*y0];
      for (int y = -recrad; y < recrad; y++) {
        float tmp1 = (R + x*cosb + y*sinb);
        float axy = R*(y*cosb - x*sinb)/tmp1 + halfw;
        //they are functions from x,y, отсчитываем от края pF
        float bxy = z*R/tmp1 + halfh - b1; // they are functions from x,y
        int iaxy = static_cast<int>(axy); // вычислим целую часть axy
        int ibxy = static_cast<int>(bxy); // вычислим целую часть bxy
        axy -= iaxy; // вычислим дробную часть axy
        bxy -= ibxy; // вычислим дробную часть bxy
        float factor = RRAnglePitch/(tmp1*tmp1);
        float add = factor
          *(conv[iaxy*wconv+ibxy]*(1-axy)*(1-bxy)
          + conv[(iaxy+1)*wconv+ibxy]*axy*(1-bxy)
          + conv[iaxy*wconv+ibxy+1]*(1-axy)*bxy
          + conv[(iaxy+1)*wconv+ibxy+1]*axy*bxy);
        #pragma omp atomic
        tFfdk[halfy0+y] += add;
      }
    }
    recTime += omp_get_wtime()-start;
    free(conv);
  }
  free(p);
  // Ffdk has inverted axes for optimization purposes. Revert it to normal.
  #pragma omp parallel for
  for(int y = 0; y < y0; y++)
    for(int x = 0; x < x0; x++)
      bitmap[y*x0+x] = Ffdk[x*y0+y];
  convTime /= omp_get_max_threads();
  recTime /= omp_get_max_threads();
  sliceTime = omp_get_wtime()-sliceTime;
  if(verbose_timing_info) {
    int dev = _Offload_get_device_number();
    printf("%2i Slice %3i  Preprocessing time = %f\n", dev, static_cast<int>(z+VertCentre), preTime);
    printf("%2i Slice %3i    Convolution time = %f\n", dev, static_cast<int>(z+VertCentre), convTime);
    printf("%2i Slice %3i Reconstruction time = %f\n", dev, static_cast<int>(z+VertCentre), recTime);
    printf("%2i Slice %3i          Slice time = %f\n", dev, static_cast<int>(z+VertCentre), sliceTime);
  }
}
