
#include "fdk.h"
#include <iostream>
using std::cout;
using std::endl;
#include <omp.h>

int main (int argc, char * argv[]) {
  int thr_host = omp_get_max_threads();
  cout << "OMP_Max_num_threads is " << thr_host << endl;
  bool use_mic = true;
  if(argc >= 2) {
    int n_thr = atoi(argv[1]);
    if(n_thr > 0) {
      for(int i = 0; i < _Offload_number_of_devices(); i++) {
        #pragma offload target(mic:i) in(n_thr)
        omp_set_num_threads(n_thr);
      }
    } else {
      use_mic = false;
    }
  }
  if (use_mic) {
    for(int i = 0; i < _Offload_number_of_devices(); i++) {
      #pragma offload target(mic:i)
        cout << "Device #" << _Offload_get_device_number()
            << ": OMP_Max_num_threads is " << omp_get_max_threads() << endl;
    }
  }
  //initialization
  FDK::Init("settings.ini", (thr_host > 1), use_mic);
  FDK::Reconstruct();
  FDK::Finalize();
}
