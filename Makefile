
####### Variables

CXX      := icpc
CXXFLAGS := -O3 -g -Wall -fopenmp -fPIC -std=c++0x -qopt-report-phase=vec -qopt-report=5
LDFLAGS  := -O3 -g -fopenmp
# -qopt-report-phase=vec -- писать отчёт о векторизации
# -qopt-report=5 -- насколько подробный (5, по опыту, оптимум)
LIBS     := libfreeimage.so

####### Files

OBJECTS  := fdk fdk_async fdk_vars ConfigParser Slice main_gen
TARGET   := code

####### Build rules

$(TARGET): $(addsuffix .o,$(OBJECTS))
	@echo "Linking executable '$@'..."
	$(CXX) $(LDFLAGS) $(LIBS) $^ -o $@

clean:
	@echo "Cleaning..."
	$(RM) *.o *.d *.optrpt $(TARGET)

####### Compile

%.o: %.cc
%.o: %.cc %.d
	@echo "Compiling $<..."
	$(CXX) -c $(CXXFLAGS) $< -o $@

####### Dependencies

%.d: %.cc
	@echo "Generating dependencies for $<..."
	@g++ -MM -MF $@ $<

include $(wildcard *.d)

.PRECIOUS: %.d

.PHONY: clean
