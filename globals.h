
#ifndef globals_h
#define globals_h

#define EDGE 1
#define DETH 257

#define ALLOC alloc_if(1) free_if(0)
#define REUSE alloc_if(0) free_if(0)
#define FREE  alloc_if(0) free_if(1)

__declspec(target(mic))
extern bool verbose_timing_info;

#endif
