
#include "fdk.h"

//! variables shared with coprocessor
__declspec(target(mic))
  int FDK::x0, FDK::y0, FDK::halfx0, FDK::halfy0,
             FDK::rap2, FDK::halfw, FDK::w,
             FDK::p3, FDK::p3p2;
__declspec(target(mic))
  float FDK::rap, FDK::R, FDK::theta, FDK::sintheta, FDK::costheta,
               FDK::HorCentre, FDK::VertCentre;
__declspec(target(mic))
  unsigned int FDK::Xmax, FDK::Ymax,
                      FDK::width, FDK::height;
__declspec(target(mic))
  float * FDK::angle,
               * FDK::filter,
               * FDK::kp,
               * FDK::Ffdk,
               * FDK::b [DETH];

//! host-only variables
//   static bool ** FDK::in_use;
  Slice * FDK::slices;

  int FDK::Z1, FDK::Z2;

  float FDK::sdd, FDK::sod, FDK::pixelsize;

  string FDK::PathToScan, FDK::PathToAngles, FDK::PathToSave;
  vector <string> FDK::files;
  double FDK::tmFDK;
  double FDK::tmOpen, FDK::tmSave, /*tmPre, tmConv, tmRec,*/ FDK::tmSlice, FDK::tmAlg;
  bool FDK::USE_HOST, FDK::USE_MIC;
  unsigned int FDK::n_mic, FDK::n_dev;
  char * FDK::mic_sig;
  omp_lock_t FDK::host_lock;
  bool ** FDK::in_use;
  int FDK::z_host, * FDK::z_mic;
__declspec(target(mic))
  bool verbose_timing_info = false;
