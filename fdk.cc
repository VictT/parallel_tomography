
#include "fdk.h"
#include "ConfigParser.h"
#include "Slice.h"
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
using std::flush;
#include <cstdlib>
#include <cmath>
#include <errno.h>
#include <dirent.h>
#include <algorithm>
#include <cstring>
#include <climits>

/*!
Descriptions of FDK functions
*/


//local function for sorting file names
bool CompareStrings (string str0, string str1) {
  return (str0.compare(str1) < 0);
}

//The modern FDK constructor
void FDK::Init(std::string settings_file, bool use_host, bool use_mic) {
  omp_set_nested(1);
  tmFDK = omp_get_wtime();
  USE_HOST = use_host;
  USE_MIC = use_mic;
  if(USE_MIC) n_mic = _Offload_number_of_devices();
  else n_mic = 0;
  n_dev = n_mic+USE_HOST;
  if(n_dev == 0) {
    cerr<<"No devices for reconstruction!"
        <<"Exiting..."<<endl;
    exit(1);
  }
  mic_sig = new char[n_mic]; //сигналы - работает на mic задача или ещё/уже нет
  z_host = -1;
  z_mic = new int[n_mic]; //номер обрабатываемого слайса
  for(int i = 0; i < n_mic; i++) {
    z_mic[i] = -1;
  }
  omp_init_lock(&host_lock); //иниц-я сигнала для хоста
  in_use = new bool*[n_dev]; //массив булевских массивов
  for(unsigned int i = 0; i < n_dev; i++)
    in_use[i] = reinterpret_cast<bool*>(calloc(DETH, sizeof(bool))); //выделяем память DETH*sizeof(bool), заполняем нулями
  memset(b, 0, DETH*sizeof(float*));//b - массив указателей на bitmap'ы входных синограмм
  ConfigParser settings(settings_file);
  PathToScan = settings.GetString("PATH_TO_SCAN");
  if (PathToScan == ""){
    cerr<<"Error! No PATH TO SCAN value"<<endl;
    exit (-1);
  }
  PathToSave = settings.GetString("PATH_TO_SAVE");
  if (PathToSave == ""){
    cerr<<"Error! No PATH_TO_SAVE value"<<endl;
    exit (-1);
  }
  PathToAngles = settings.GetString("PATH_TO_ANGLES");
  if (PathToAngles == ""){
    cerr<<"Warning! No PATH_TO_ANGLES value"<<endl;
  }
  x0 = settings.GetInt("X",600);
  y0 = settings.GetInt("Y",600);
  sod = settings.GetDouble("SOD", 142.4);
  sdd = settings.GetDouble("SDD", 235.8);
  rap = settings.GetDouble("RAP", 257.695);
  pixelsize = settings.GetDouble("PIXEL_SIZE", 0.055);
  HorCentre = settings.GetInt("HORIZONTAL_CENTRE", INT_MAX);
  if (HorCentre == INT_MAX){
    cerr<<"Error! No value for HORIZONTAL_CENTRE"<<endl;
    exit (-1);
  }
  VertCentre = settings.GetInt("VERTICAL_CENTRE", INT_MAX);
  if (VertCentre == INT_MAX){
    cerr<<"Error! No value for VERTICAL_CENTRE"<<endl;
    exit (-1);
  }
  Z1 = settings.GetInt("Z1", INT_MAX)-VertCentre;
  Z2 = settings.GetInt("Z2", INT_MAX)-VertCentre;
  if (((Z1 == INT_MAX)||(Z2 == INT_MAX))) {
    cerr<<"Error! Invalid values for Z1 and Z2"<<endl;
    exit (-1);
  }
  // initialize the FreeImage library
  FreeImage_Initialise(TRUE);
  slices = new Slice[1+Z2-Z1];

  tmOpen = 0; tmSave = 0; // tmConv = 0; tmRec = 0; tmSlice = 0;

  rap2 = static_cast<int>(rap);
  halfx0 = 0.5*x0;
  halfy0 = 0.5*y0;

  theta = atan(sod/(fabs(HorCentre - rap)*pixelsize));
  sintheta = sin(theta);
  costheta = cos(theta);
  R = sdd/(pixelsize*sintheta*sintheta);
//++++++++++++++++++++++++++++++++++++++++++++++
  DIR * dir;
  struct dirent * ent;
  //open PathToScan
  if (!(dir=opendir(PathToScan.c_str()))) {
    cerr<<"Can't open directory "<<PathToScan<<endl;
    exit(-1);
  }
  //diagnistic
  errno = 0;
  while ((ent=readdir(dir))) {
    if (strcmp (ent->d_name,".") != 0 && strcmp (ent->d_name,"..") != 0){
      files.push_back(ent->d_name);
    }
    //reset errno
    errno = 0;
  }
  // using function as comp
  std::sort (files.begin(), files.end(), CompareStrings);
  if(files.size() != DETH) {
    cerr<<"Wrong input! number of input files != detector size"<<endl
        <<"Exiting..."<<endl;
    exit(1);
  }
//++++++++++++++++++++++++++++++++++++++++++++++
  SingleCalculations();
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) signal(&mic_sig[i]) \
      in(x0, y0, halfx0, halfy0, rap2, halfw, w, p3, p3p2, rap, HorCentre, VertCentre, \
         R, theta, sintheta, costheta, Xmax, Ymax, width, height)
  }
}
void FDK::SingleCalculations() {
  // Получу размеры картинки, чтобы выделить память и заполнить функцию-фильтр
  // image format
  FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
  // pointer to the image, once loaded
  FIBITMAP * dib(0);
  // Make full filename = Directory name + filename
  string Filename = PathToScan+"/"+files[0];
  // check the file signature and deduce its format
  fif = FreeImage_GetFileType(Filename.c_str(), 0);
  if (fif != FIF_TIFF) { cerr<<"Warning! Type of input data is not tiff!"<<endl; }
  // check that the plugin has reading capabilities and load the file
  if (FreeImage_FIFSupportsReading(fif)) {
    dib = FreeImage_Load(fif, Filename.c_str(), TIFF_NONE);
  }
  if (!dib) { cerr << "Error while loading file" << endl; }
  width = FreeImage_GetWidth(dib);
  height = FreeImage_GetHeight(dib);
  FreeImage_Unload(dib);
  Xmax = static_cast<int>(width*sintheta);
  Ymax = height;
  w = abs(Xmax-2*rap) + Xmax; //new width of the detector
  halfw = 0.5*(w-1); //halfwidth
  p3 = w;
  p3p2 = p3*Ymax;
//   p = reinterpret_cast<float*>(calloc(Ymax*w*DETH, sizeof(float)));
  Ffdk = reinterpret_cast<float*>(malloc(x0*y0*sizeof(float)));//массив под выходную картинку
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        in(Ffdk: length(x0*y0) ALLOC)
  }
  kp = reinterpret_cast<float*>(calloc(w, sizeof(float)));
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        in(kp: length(w) ALLOC)
  }
  angle = (float*)calloc(Ymax+1, sizeof(float));
  if (PathToAngles == "") {
    for (unsigned int B = 0; B <= Ymax; B++) {
      angle[B] = 2*M_PI*B/Ymax;
    }
  } else {
    FILE * aFile;
    char buf[15];
    int nentries=0; //number of angles in file (should be = number of input files)
    aFile = fopen (PathToAngles.c_str() , "r");
    if (aFile == NULL) {
      perror ("Error opening file with angles");
    } else {
      while (! feof (aFile)) {
        if(fgets (buf, 15, aFile) != NULL) {
          angle[nentries] = atof(buf);
          static const float angle0 = angle[0];
          angle[nentries] += fabs(angle0);
          angle[nentries] = 2*M_PI*angle[nentries]/360;
          nentries++;
        }
      }
      angle[Ymax] = 2*M_PI;//last angle should be = 2pi
      fclose (aFile);
    }
    if (nentries != Ymax) {cout<<"Warning! Number of angles != number of files in directory"<<std::endl;}
  }
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        in(angle: length(Ymax+1) ALLOC)
  }
  filter = (float*)calloc(4*halfw+1, sizeof(float)); //ramp filter
//======================Filling the ramp filter======================//
  float tau = 0.1*pixelsize*sintheta;// cm
  #pragma omp parallel for
  for (int i = 1; i < 2*halfw; i += 2) {
    filter[2*halfw + i] = -1.0/(M_PI*M_PI*tau*i*i);
    filter[2*halfw - i] = -1.0/(M_PI*M_PI*tau*i*i);
    filter[2*halfw + i + 1] = 0;
    filter[2*halfw - i - 1] = 0;
  }
  filter[2*halfw] = 0.25/tau; //1.0/(4tau)
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        in(filter: length(4*halfw+1) ALLOC)
  }
//===========================Ramp filter is filled=============================//
}

void FDK::Finalize() {
  #pragma omp barrier
  for (unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_wait target(mic:i) wait(&mic_sig[i])
  }
  for(unsigned int i = 0; i < n_mic; i++) {
    if(z_mic[i] >= 0) {
      #pragma omp task untied
        slices[z_mic[i]].Save(PathToSave, z_mic[i]+Z1+VertCentre);
    }
  }
  if(USE_HOST && z_host >= 0) {
    #pragma omp task untied
      slices[z_host].Save(PathToSave, z_host+Z1+VertCentre);
  }
  CloseSynograms();
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        nocopy(filter: length(4*halfw+1) FREE)
  }
  free(filter);
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        nocopy(angle: length(Ymax+1) FREE)
  }
  free(angle);
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        nocopy(Ffdk: length(x0*y0) FREE)
  }
  free(Ffdk);
  for(unsigned int i = 0; i < n_mic; i++) {
    #pragma offload_transfer target(mic:i) \
        nocopy(kp: length(w) FREE)
  }
  free(kp);
//   free(p);
  omp_destroy_lock(&host_lock);
  delete [] mic_sig;
  delete [] z_mic;
  for(unsigned int i = 0; i < n_dev; i++)
    free(in_use[i]);
  delete [] in_use;
  delete [] slices;
  // release the FreeImage library
  FreeImage_DeInitialise();
  tmFDK = omp_get_wtime()-tmFDK;
  cout << "     File open time = " << tmOpen << endl;
//   cout << "   Convolution time = " << tmConv << endl;
//   cout << "Reconstruction time = " << tmRec << endl;
//   cout << "    Processing time = " << tmSlice << endl;
  cout << "     File save time = " << tmSave << endl;
  cout << "     Total Alg time = " << tmAlg << endl;
  cout << "     Total FDK time = " << tmFDK << endl;
}
void FDK::CloseSynograms() {
  for (int I = 0; I < DETH; I++) {
    for (unsigned int d = 0; d < n_mic; d++) {
      if(in_use[d+USE_HOST][I]) {
        #pragma offload_transfer target(mic:d) \
            nocopy(b[I]: length(width*height) FREE)
      }
    }
    if(b[I])
      free(b[I]);
  }
}

void FDK::ReconstructSlice(int z, int b1, int b2) {
//   double sliceTime = omp_get_wtime();
  slices[z-Z1].Allocate(x0, y0);
  bool task_created = false;
  while(!task_created) {
    for(unsigned int i = 0; i < n_mic; i++) {
      if(_Offload_signaled(i, &mic_sig[i])) {//если есть сигнал,что mic отработал
        OpenSynograms(i+USE_HOST, b1, b2);
        cout<<"Reconstructing slice "<<static_cast<int>(z+VertCentre)<<" on MIC"<<i<<endl;
        float * bits = slices[z-Z1].GetBits();
        #pragma offload target(mic:i) signal(&mic_sig[i]) \
            in(z, b1, b2) out(bits: length(x0*y0))
          ReconstructBitmap(z, b1, b2, bits);
        if(z_mic[i] >= 0) {//если есть какие-то уже восстановленные сечения
          int tmp = z_mic[i];
          //то создаём новое задание
          #pragma omp task untied
            slices[tmp].Save(PathToSave, static_cast<int>(tmp+Z1+VertCentre)); //на сохранение сечения
        }
        z_mic[i] = z-Z1; //меняем номер актуального для пересохранения сечения
        task_created = true;
      }
      if(task_created) break;
    }
    if(task_created) break;
    if(USE_HOST && omp_test_lock(&host_lock)) {
      OpenSynograms(0, b1, b2);
      #pragma omp task untied
      {
        cout<<"Reconstructing slice "<<static_cast<int>(z+VertCentre)<<" on host"<<endl;
        ReconstructBitmap(z, b1, b2, slices[z-Z1].GetBits());
        omp_unset_lock(&host_lock);
      }
      task_created = true;
      if(z_host >= 0) {
        int tmp = z_host;
        #pragma omp task untied
          slices[tmp].Save(PathToSave, static_cast<int>(tmp+Z1+VertCentre));
      }
      z_host = z-Z1;
    }
    if(task_created) break;
    usleep(10);
  }
//   tmRec += recTime;
//   sliceTime = omp_get_wtime()-sliceTime;
//   tmSlice += sliceTime;
//   if(verbose_timing_info) {
//     cout << "   Convolution time = " << convTime << endl;
//     cout << "Reconstruction time = " << recTime << endl;
//     cout << "         Slice time = " << sliceTime << endl;
//   }
}
void FDK::Reconstruct() {
  tmAlg = omp_get_wtime();
  #pragma omp parallel
  {
  #pragma omp single nowait
  {
    for (int z = Z1; z <= Z2; z++) { //reсonstructing slices from z1 to z2
      int b1, b2; /*! b1, b2 are limits of integration */
//       cerr<<"Reconstructing "<<z+VertCentre<<" slice"<<endl;
    //============Let's calculate the number of useful files - integration limits for z slice================//
      if (z < 0) {
        b2 = static_cast<int>(z*R/(R +(halfx0)*sqrt(2)) + 0.5*(abs(DETH-2*VertCentre) + DETH - 1))+1;
        b1 = static_cast<int>(z*R/(R -(halfx0)*sqrt(2)) + 0.5*(abs(DETH-2*VertCentre) + DETH - 1));
      }
      else {
        b1 = static_cast<int>(z*R/(R +(halfx0)*sqrt(2)) + 0.5*(abs(DETH-2*VertCentre) + DETH - 1));
        b2 = static_cast<int>(z*R/(R -(halfx0)*sqrt(2)) + 0.5*(abs(DETH-2*VertCentre) + DETH - 1))+1;
      }
      if (b1 < 0) {b1 = 0;}
      if (b2 > DETH) {b2 = DETH;}
      ReconstructSlice(z, b1, b2);
    } // from Z1 to Z2
  }
  }
  tmAlg = omp_get_wtime()-tmAlg;
//   tmSave = omp_get_wtime();
//     slices[z-Z1].Save(PathToSave, static_cast<int>(z-1+VertCentre)); //save image z
//   tmSave = omp_get_wtime()-tmSave;
}
void FDK::OpenSynograms(unsigned int dev, int b1, int b2) {
  double opentime = omp_get_wtime();
//   cout<<"b1 = "<<b1<<", b2 = "<<b2<<endl;
/*  cout<<"AT START"<<endl<<"*|";
  for(int i = 0; i < 100; i++)
    cout<<(b[i]?1:0);
  cout<<endl;
  for (unsigned int d = 0; d < n_dev; d++) {
    cout<<d<<"|";
    for(int i = 0; i < 100; i++)
      cout<<in_use[d][i];
    cout<<endl;
  }*/
  for (int I = 0; I < b1; I++) {
    if (!in_use[dev][I]) continue;
    in_use[dev][I] = false;
    int mic_dev = dev-USE_HOST;
    if(mic_dev >= 0) {
      #pragma offload_transfer target(mic:mic_dev) \
          nocopy(b[I]: length(width*height) FREE)
    }
  }
  for (int I = 0; I < b1; I++) {
    if(!b[I]) continue;
    bool in_use_any = false;
    for (unsigned int d = 0; d < n_dev; d++) {
      if(in_use[d][I]) in_use_any = true;
    }
    // определили in_use_any
    if(in_use_any) break;
    if(verbose_timing_info) cout<<"Closing "<<I<<endl;
    free(b[I]);
    b[I] = 0;
  }
/*  cout<<"AFTER CLOSE"<<endl<<"*|";
  for(int i = 0; i < 100; i++)
    cout<<(b[i]?1:0);
  cout<<endl;
  for (unsigned int d = 0; d < n_dev; d++) {
    cout<<d<<"|";
    for(int i = 0; i < 100; i++)
      cout<<in_use[d][i];
    cout<<endl;
  }*/
  //Loop for all pictures in the directory
  for (int I = b1; I < b2; I++) {
    if(b[I]) continue;
    if(verbose_timing_info) cout<<"Opening "<<I<<endl;
    b[I] = reinterpret_cast<float*>(malloc(width*height*sizeof(float)));
    //Make full filename = Directory name + filename
    string Filename = PathToScan+"/"+files[I];
    //check the file signature and deduce its format
    FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(Filename.c_str(), 0);
    if (fif != FIF_TIFF) {
      cerr<<"Warning! Type of input data is not tiff!"<<endl;
    }
    //check that the plugin has reading capabilities and load the file
    if (!FreeImage_FIFSupportsReading(fif)) {
      cerr << "Error! FreeImage doesn't support TIFF!" << endl;
    }
    //pointer to the image, once loaded
    FIBITMAP * dib = FreeImage_Load(fif, Filename.c_str(), TIFF_NONE);
    if (!dib) {
      cerr << "Error while loading file!" << endl;
    }
    //retrieve the image data
    float * bits = reinterpret_cast<float*>(FreeImage_GetBits(dib));
    if ((bits == 0) || (width == 0) || (height == 0)) {
      cerr << "File has width or height equal to zero or NULL pointer" << endl;
    }
    memcpy(b[I], FreeImage_GetBits(dib), width*height*sizeof(float));
    FreeImage_Unload(dib);
  }
  for (int I = b1; I < b2; I++) {
    if(in_use[dev][I]) continue;
    int mic_dev = dev-USE_HOST;
    if(mic_dev >= 0) {
      #pragma offload_transfer target(mic:mic_dev) \
          in(b[I]: length(width*height) ALLOC)
    }
    in_use[dev][I] = true;
  }
/*  cout<<"AFTER OPEN"<<endl<<"*|";
  for(int i = 0; i < 100; i++)
    cout<<(b[i]?1:0);
  cout<<endl;
  for (unsigned int d = 0; d < n_dev; d++) {
    cout<<d<<"|";
    for(int i = 0; i < 100; i++)
      cout<<in_use[d][i];
    cout<<endl;
  }*/
}
