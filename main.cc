
#include "fdk.h"
#include <iostream>
#include <omp.h>

using std::cout;
using std::endl;

int main (int argc, char * argv[]) {
  cout << "OMP_Max_num_threads is " << omp_get_max_threads() << endl;
  //initialization
  FDK::Init("settings.ini", 0, 1);
  //calculations and saving
  FDK::Reconstruct();
  FDK::Finalize();
}
